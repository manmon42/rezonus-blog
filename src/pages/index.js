import React from "react"
import Layout from "../components/layout"
import PostList from "../components/post-list"
import { Helmet } from "react-helmet"

const Index = () => (
  <div>
    <Helmet>
      <meta charSet="utf-8" />
      <title>Rezonus</title>
      {/* <link rel="canonical" href="http://mysite.com/example" /> */}
      <html lang="en" />
    </Helmet>
    <Layout>
      <PostList />
    </Layout>
  </div>
)

export default Index
