import React from "react"
import { Helmet } from "react-helmet"
import { Link } from "gatsby"

const FourOhFour = () => (
  <div>
    <Helmet>
      <meta charSet="utf-8" />
      <title>Rezonus</title>
      {/* <link rel="canonical" href="http://mysite.com/404" /> */}
      <html lang="en" />
    </Helmet>
    <h1>Oops! This is nothing!</h1>
    <Link to={"/"}>Home</Link>
  </div>
)

export default FourOhFour
