import React from "react"
import styled from "styled-components"
import { rhythm } from "../utils/typography"
import { Link, useStaticQuery, graphql } from "gatsby"

const PostTitle = styled.div`
  font-size: ${rhythm(1.25)};
  color: #272727;
`

const PostDate = styled.div`
  margin-left: 5px;
  color: #101010;
`

const PostHeaderContainer = styled.div`
  display: flex;
  align-content: center;
  align-items: center;
`

const StyledLink = styled(Link)`
  color: #101010;
  outline-width: 1px;
  outline-style: "solid";
  outline-color: "gray";
`

const PostSubtitle = styled.p`
  font-size: 1.2rem;
  margin-bottom: ${rhythm(0.5)};
`

const PostList = () => {
  const data = useStaticQuery(
    graphql`
      query {
        allMarkdownRemark {
          edges {
            node {
              frontmatter {
                title
                subtitle
                date(formatString: "MM-DD-YYYY")
              }
              fields {
                slug
              }
              id
              excerpt
            }
          }
        }
      }
    `
  )

  return (
    // <div>{data.query.allMarkdownRemark.edges[0].node.frontmatter.title}</div>
    <div>
      {data.allMarkdownRemark.edges.map(({ node }) => (
        <div key={node.id}>
          <StyledLink to={node.fields.slug}>
            <PostHeaderContainer>
              <PostTitle>{node.frontmatter.title}</PostTitle>
              <PostDate>- {node.frontmatter.date}</PostDate>
            </PostHeaderContainer>
            <PostSubtitle>{node.frontmatter.subtitle}</PostSubtitle>
            <p>{node.excerpt}</p>
          </StyledLink>
        </div>
      ))}
    </div>
  )
}

export default PostList
