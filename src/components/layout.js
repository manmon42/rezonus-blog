import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import { rhythm } from "../utils/typography"
import { Link, useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import backgroundTexture from "../assets/background-texture.svg"

const Background = styled.div`
  padding: ${rhythm(2)};
  background-image: url(${backgroundTexture}),
    linear-gradient(
      349deg,
      rgba(173, 175, 55, 1) 0%,
      rgba(141, 154, 10, 1) 12%,
      rgba(4, 67, 137, 1) 100%
    );
  background-size: 300px, auto;
  background-attachment: fixed;
  background-blend-mode: overlay;
  min-height: 100vh;
  display: flex;
  justify-content: center;
`

const Container = styled.div`
  padding: ${rhythm(1)};
  background-color: whitesmoke;
  display: inline-block;
  border-radius: 3px;
  box-shadow: 10px 10px 15px 0px rgba(0, 0, 0, 0.75);
`

const Content = styled.div`
  margin: 3rem auto;
  max-width: 1000px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const Header = styled.div`
  margin: auto;
  display: flex;
  flex-direction: row;
  align-items: center;
  align-content: center;
`

const Name = styled.div`
  font-size: ${rhythm(1.25)};
  color: #101010;
  max-height: ${rhythm(1.25)};
`

const Tagline = styled.div`
  font-size: ${rhythm(0.75)};
  color: #272727;
`
const HeaderText = styled.div`
  margin-left: ${rhythm(0.5)};
  display: flex;
  flex-direction: column;
`

const Home = styled(Link)`
  font-size: ${rhythm(1.25)};
  color: #101010;
  text-decoration: none;
`

const FlexBuffer = styled.div`
  flex-grow: 2;
`

const Layout = ({ children }) => {
  const data = useStaticQuery(
    graphql`
      query {
        file(relativePath: { eq: "profile.png" }) {
          id
          childImageSharp {
            fixed(width: 60, height: 60) {
              ...GatsbyImageSharpFixed
            }
          }
        }
      }
    `
  )

  return (
    <Background>
      <Container>
        <Header>
          <Img
            style={{ borderRadius: 50 }}
            fixed={data.file.childImageSharp.fixed}
          />
          <HeaderText>
            <Name>Max Starr</Name>
            <Tagline>Student, Web Developer for hire</Tagline>
          </HeaderText>
          <FlexBuffer />
          <Home to={"/"}>Home</Home>
        </Header>
        <Content>{children}</Content>
      </Container>
    </Background>
  )
}

Layout.propTypes = {
  children: PropTypes.node,
}

export default Layout
