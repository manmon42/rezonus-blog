---
title: "Building the LLJSVMIDE"
subtitle: "Part One: Setup and Registers"
date: "2019-11-17"
---

# Preface

First off, all credit for the underlying virtual machine goes to the [Low Level Javascript YouTube channel](https://www.youtube.com/channel/UC56l7uZA209tlPTVOJiJ8Tw). I have modified that code somewhat as will be outlined in this blog series. Any code in those files that is not explicitly a modification of mine is not my code.

# Backstory

I first encountered the LLJS channel when the creator, Francis Stokes, [posted a video of his on reddit](https://www.reddit.com/r/node/comments/cgsfsu/how_parser_combinators_work/). I found the videos instantly engaging and educational. Honestly these made more sense than half my college courses. So, a few episodes into his series on making a Javascript virtual machine, I saw an opportunity to practice my front end dev skills and enrich my understanding of the underlying mechanics.

As some may have guessed from the look of this blog, I am not a designer. It's an area I personally need to grow in. I'm working on it. So keep that in mind as this series progresses. This thing is function over form for the most part.

Now! For the good stuff!

# Starting off

I started the project with just god old `create react app`. I don't believe any features offered by more complex react frameworks are necessary for this project, at least in its current form.

Next, I copied over the existing LLJSVM code into my fresh app in its own folder.

I will be using the [Material-UI](https://material-ui.com/) framework for this project as a crutch for my lack of design chops. All styling syntax is because of this choice. View on anything besides a 16:9 display at your own risk. (I'm sorry)

Boring setup done, it's time to start the real work.

# Registers

The first module I implemented was the register display.

The first problem to be solved in this implementation was getting the register values out of the `cpu` and into the state of my react app.

The solution turned out to be fairly trivial. Since `cpu` is a class and the registers are just properties of that class, all I had to do was export the instance of `cpu` from `index` and import it in my main app component.

###### lljsvm/index.js

```jsx
export { cpu }
```

###### App.js

```jsx
import { cpu } from "./lljsvm/index"
```

I now have access to the whole `cpu` instance in my react app. I end up passing the whole instance down to the `jsx±<Registers />` component.

###### App.js

```jsx
import Registers from "./componenets/registers";

...

<Registers cpu={cpu} />
```

In the register component, I now have to get all the values of the registers and display them in a semi sensible manner.

Fortunately for me, `cpu` has a method to read the value of a register by name and has an array of all the names as another property. This makes looping through each of them to construct the list nearly trivial.

###### components/registers.js

```jsx
const registerDisplayElems = cpu.registerNames.map((name) => {
    return (
      // some formatted elements
    );
  });
```

`Array.map()` is incredibly useful in react, it's not even funny.

Then it's just a matter of formatting the data. I ended up copying more from what was already done in the LLJSVM project. This is where I learned about the `String.padStart()` method, incredible.

###### components/registers.js

```jsx
const registerDisplayElems = cpu.registerNames.map(name => {
  return (
    <div key={name} className={classes.register + " " + classes[name]}>
      {name.padEnd(3, " ")}:{" 0x"}
      {cpu
        .getRegister(name)
        .toString(16)
        .padStart(4, "0")}
    </div>
  )
})
```

Let's go through this line by line, mostly.

```jsx
<div key={name} className={classes.register + " " + classes[name]}>
```

Here I'm assigning each sibling in this array a unique key, that being the name of the register. This is a react thing the compiler yells at me if I don't do. Next, I'm setting the classes for each register line. The whole `classes.name` thing is from the Material-UI library. You can read more about it in [the docs](https://material-ui.com/styles/basics/). All it's doing is giving each element the class `register` and a unique class of its own register name. I'm doing anything with them here, but it will be important later.

```jsx
{name.padEnd(3, " ")}:{" 0x"}
```

This line writes out the name of the register and makes sure they each take up three characters by padding the end with whitespace where needed. This only works up to one space in react with this method, as will be explored later, but for now that doesn't matter as there is, as of this writing, never more than a one space difference in the register names. It then sets up for the next line by adding some prefix characters for the 16 bit register values. Since, by the convention I'm using, 16 bit numbers are preceded by a `0x` to signal that they are, in fact, 16 bit numbers.

```jsx
{
  cpu
    .getRegister(name)
    .toString(16)
    .padStart(4, "0")
}
```

This is the real crunch of this component. THis first calls the `getRegister()` method on the passed `cpu` instance which returns the value of the register at that time. It returns it in decimal so the next line calls the `toString()` method on the returned value. The `16` passed to it is, as you may have guessed, the desired number base to display in. THe last line calls the now familiar `padStart()` method on the now base 16 number string. It ensures that all values are length 4 by adding appropriate leading zeros.

Alright! That's register's done! There's a few styling specifics that don't matter too much. Namely the `<Paper>` element wrapper for the whole thing. The full code can be found in [the repo](https://github.com/manmon42/lljsvm-ide/blob/master/src/componenets/registers.js)

And that's all for this part. Next part will be dedicated to the memory and stack displays. (Hint, they're basically the same thing).
