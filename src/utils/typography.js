import Typography from "typography"
import FairyGatesTheme from "typography-theme-fairy-gates"

const typography = new Typography(FairyGatesTheme)

export default typography
export const rhythm = typography.rhythm
