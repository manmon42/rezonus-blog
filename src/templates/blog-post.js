import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import { graphql } from "gatsby"
import Layout from "../components/layout"

const PostContent = styled.div`
  width: 100%;
`

const BlogPost = ({ data }) => {
  const post = data.markdownRemark

  return (
    <Layout>
      <PostContent>
        <h1>{post.frontmatter.title}</h1>
        <p>{post.frontmatter.subtitle}</p>
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
      </PostContent>
    </Layout>
  )
}

BlogPost.propTypes = {
  data: PropTypes.object,
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        subtitle
      }
    }
  }
`
export default BlogPost
